//permissions needed: ["EMIT_LOG","INFO_WEBHOSTING","DOCROOT_ENTRIES"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "git"

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');
    const MError = me.Error

    const GitLib = require("models/git.js")

    var knex

    config.defaults({
      "rsa_private_key_bitlength": 2048,
      "cache_webhosting_info_seconds": 3600,
      "cleanup_webhosting_info_ms": 10*60*1000, // 10 minutes
    })

    Array("cmd_wrapper", "ssh_deployment_keys_directory", "mc_primary_domain", "mc_server_name").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })

    config.appRestPrefix = "/git"

    var app = me.Express(config, moduleOptions);
    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    // external service providers will call this route
    router.RegisterPublicApis(["/triggers/", "/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    app.MonsterInfoWebhosting = require("MonsterInfo").webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer});
    app.GetDocroot = app.GetRelayerMapiPool;

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)

    var wieRef = {};
    const wieLib = require("Wie");
    router.use("/wie", wieLib.getSubrouter(app, wieRef));


    router.get("/:webhosting_id/info", getWebhosting, function(req,res,next){
       return req.sendPromResultAsIs(req.GitLib.GetGitInfoForWebhosting())
    })

    router.post("/:webhosting_id/ssh", getWebhosting, function(req,res,next){
       return req.sendOk(req.GitLib.ChangeSshConfig(req.body.json))
    })


    Array("id","name").forEach(cat=>{
      var name = "GetGitBy_"+cat
      function getGitByPromise(req){
         return req.GitLib[name](req.params.git_id)
           .then(git=>{
              req.git = git
              return Promise.resolve()
           })

      }

      function getGitBy(req,res,next){
       return getGitByPromise(req)
         .then(()=>{
            next()
         })

      }

      function getWebhostingGitTrx(callback, rejectIfExpired) {
         return getWebhostingTrx(function(req,res,next){
            return getGitByPromise(req)
               .then(()=>{
                  return callback(req,res,next)
               })
         }, rejectIfExpired)
      }

      function getWebhostingGitBranchTrx(callback) {
         return getWebhostingGitTrx(function(req,res,next){
            return getBranchByNamePromise(req)
               .then(()=>{
                  return callback(req,res,next)
               })
         })
      }

      if(cat == "id"){
          router.post("/triggers/:webhosting_id/:git_id", getWebhostingReject(true), getGitBy, function(req,res,next){

              var p = req.git.Pull(req.body.json)
                .then(h=>{
                    return cleanupTaskResult(h)
                })

              return req.sendPromResultAsIs(p)

          })

      }


      router.route("/:webhosting_id/"+cat+"/:git_id")
        .post(getWebhostingGitTrx(function (req, res, next) {
             return req.sendOk(
                req.git.Change(req.body.json)
                .then(()=>{
                    app.InsertEvent(req, {e_event_type: "git-change", e_other:true})
                })
              )
        }))
        .get(getWebhosting, getGitBy, function (req, res, next) {
             req.sendResponse(req.git.Cleanup())
        })
        .delete(getWebhostingGitTrx(function (req, res, next) {
             return req.sendOk(req.git.Delete(req)
                .then(()=>{
                    app.InsertEvent(req, {e_event_type: "git-removed"})
                })
             )
        }))




      Array("setdocroot","log","pull","revert","checkout","clean","clone").forEach(q=>{

          router.post("/:webhosting_id/"+cat+"/:git_id/branch/:branch_name/"+q, getWebhosting, getGitBy, getBranchByName, function(req,res,next){
              return req.git[q](req.body.json)
                .then(r=>{
                     app.InsertEvent(req, {e_event_type: "branch-op", e_other:extend({},req.params,req.body.json,{c:q})})
                     return req.sendResponse(r);
                })


          })

      })

      router.route("/:webhosting_id/"+cat+"/:git_id/branch/:branch_name")
         .post( getWebhostingGitBranchTrx(function(req,res,next){
             return req.sendOk(req.branch.Change(req.body.json))
         }))
         .get( getWebhosting, getGitBy, getBranchByName, function(req,res,next){
             req.sendResponse(req.branch.Cleanup())
         })
         .delete( getWebhostingGitBranchTrx(function(req,res,next){
             return req.sendOk(
                req.branch.Delete(req)
                .then(()=>{
                    app.InsertEvent(req, {e_event_type: "branch-removed", e_other:true})
                })
             )
         }))

      router.route("/:webhosting_id/"+cat+"/:git_id/branches")
         .get(getWebhosting, getGitBy, function (req, res, next) {
             return req.sendPromResultAsIs(req.git.GetBranches())
         })
         .put(getWebhostingGitTrx(function(req,res,next){
            return getGitByPromise(req)
              .then(()=>{
                  return req.git.InsertBranch(req.body.json, req)
              })
              .then(h=>{
                  return cleanupTaskResult(h)
              })
              .then(r=>{
                  app.InsertEvent(req, {e_event_type: "branch-create", e_other:true})
                  return req.sendResponse(r);
              })

        }, true))


    })

    router.get("/:webhosting_id/tasks", getWebhosting, function(req,res,next){
       // this one returns the currently active tasks
       req.sendResponse(req.GitLib.GetActiveTasksOfWebhosting())
    })

    router.get("/:webhosting_id/ssh_deployment_key", getWebhosting, doSshDeploymentKey)
    router.post("/:webhosting_id/ssh_deployment_key", getWebhosting, doSshDeploymentKey)


    router.route("/:webhosting_id")
      .get(getWebhosting, function(req,res,next){


              req.GitLib.GetAllGitsOfWebhosting()
              .then(all=>{
                 req.sendResponse(all.Cleanup())
              })


      })
      .put(getWebhostingTrx(function(req,res,next){
          return req.GitLib.Insert(req.body.json, req)
               .then(r=>{
                    app.InsertEvent(req, {e_event_type: "git-create", e_other:true})
                    return req.sendResponse(r);
               })

      }, true))
      .delete(function(req,res,next){
          // this is implemented this way so that the object can be deleted even if the webhosting api does not have it anymore
          var gitlib = GitLib({wh_id:req.params.webhosting_id}, knex, app)
          return req.sendOk(

                      gitlib.DeleteAllGitsOfWebhosting(req)
                      .then(()=>{
                         return gitlib.DeleteFilesAssociatedWithWebhosting()
                      })
                       .then(()=>{
                            app.InsertEvent(req, {e_event_type: "git-removed", e_other:true})
                       })

            )
      })


    router.get("/", function(req,res,next){

         return req.sendPromResultAsIs(GitLib.GetAllGits(knex, app))
    })



    app.Prepare = function() {
       knex = require("MonsterKnex")(config)

       wieRef.wie = require("lib-wie.js")(app, knex);
       wieLib.transformWie(app, wieRef.wie);

       return knex.migrate.latest()
    }

    app.Cleanup = function() {

       var fn = config.get("db").connection.filename
       if(!fn) return Promise.resolve()

       const del = require('MonsterDel');
       return del(path.join(app.config.get("ssh_deployment_keys_directory"), "12345", "**"),{"force": true}) // these files are from unit testing
         .then(()=>{
             return fs.unlinkAsync(fn).catch(x=>{}) // the database might not exists, which is not an error in this case

         })

    }

    return app

    function getBranchByNamePromise(req) {
       return req.git.GetBranchByName(req.params.branch_name)
         .then(branch=>{
            req.branch = branch
            return Promise.resolve()
         })

    }

    function getBranchByName(req,res,next) {
       return getBranchByNamePromise(req)
         .then(()=>{
            next()
         })
    }

    function getWebhostingPromise(req,knex, rejectIfExpired) {
       return app.MonsterInfoWebhosting.GetInfo(req.params.webhosting_id)
         .then(wh=>{
            if((rejectIfExpired)&&(wh.wh_is_expired)) throw new MError("WEBHOSTING_IS_EXPIRED")
            req.webhosting = wh
            req.GitLib = GitLib(wh, knex, app)
            return Promise.resolve()
          })
    }

    function getWebhostingReject(rejectIfExpired) {
       return function(req,res,next){
           return getWebhostingPromise(req, knex, rejectIfExpired)
              .then(()=>{

                next()
             })
       }
    }

    function getWebhosting(req,res,next) {
       return getWebhostingReject()(req,res,next)
    }

    function getWebhostingTrx(callback, rejectIfExpired) {
        return function(req,res,next) {
            return knex.transaction(function(trx){

                return getWebhostingPromise(req, trx, rejectIfExpired)
                  .then(()=>{
                      return callback(req,res,next)
                  })

            })

        }
    }

    function cleanupTaskResult(h){
        if(typeof h == "object")
           delete h.executionPromise
        return Promise.resolve(h)
    }

    function doSshDeploymentKey(req,res,next){
       return req.sendPromResultAsIs(req.GitLib.GetSshDeploymentKey(req.body.json))
    }

}
