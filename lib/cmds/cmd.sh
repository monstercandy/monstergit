#!/bin/bash

set -e

DIR=$(dirname $0)

unshare -m -- "$DIR/cmd-unshared.sh" $@
