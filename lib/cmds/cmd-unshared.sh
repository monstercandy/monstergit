#!/bin/bash

set -e

USER_ID=$1
shift

STORAGE_PATH=$1
shift

LOCAL_PATH=$1
shift

WEB_PATH="$STORAGE_PATH/$LOCAL_PATH"


mount --bind "$HOME/passwd" /etc/passwd
chroot "--userspec=$USER_ID:65531" / /bin/bash <<EOF


mkdir -p "$WEB_PATH" || true
cd "$WEB_PATH"

# the rest is the git command itself
echo "$@"
$@

EOF

umount /etc/passwd
