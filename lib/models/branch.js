var lib = module.exports = function(git, knex, app) {

    var wh_id = git.GetWebhostingId()

    var ValidatorsLib = require("MonsterValidators")
    var Common = ValidatorsLib.array()
  	var vali = ValidatorsLib.ValidateJs()


      vali.validators.branchHasThePath = function(value, options, key, attributes, glob) {
          if(!value) return "path missing"
          if((options.branch.b_repo_path1 != value)&&(options.branch.b_repo_path2 != value)) {
            return "invalid path"
          }
      }

      vali.validators.notTheSame = function(value, options, key, attributes, glob) {
          if(!value) return
          if((attributes[options] == value)) return "pathes cannot be the same"
      }

      vali.validators.isBlueGreen = function(value, options, key, attributes, glob) {
         if(attributes[key]){
           options.validators.b_repo_path2.presence = true
         }
      }

      vali.validators.isRealDomainHost = function(value, options, key, attributes) {
        if(!value) return

        return new vali.Promise(function(resolve) {

            try{

              var userInput = lib.DomainHostStringsToHash(value)

              app.GetDocroot().getAsync("/s/[this]/docrootapi/docroots/hostentries/"+options.wh_id)
                .then(d=>{
                   var docrootOfficial = d.result

                   if(!validateDocroots(docrootOfficial, userInput)) {
                     console.error("invalid domain/host specified", docrootOfficial, userInput);
                     return resolve("invalid domain/host specified")
                   }

                   value = attributes[key] = lib.HashToDomainHostStrings(userInput)
                   resolve()
                })


            } catch(x) {
               resolve(x.message)
            }
        })
      }

    function getValidators() {
      var validators = {
          "b_branch": {presence: true, isString: {ascii: true}},
          "b_pull_single_branch_only": { default: {value: true}, isBooleanLazy: true},
          "b_blue_green": {default:{value: false}, isBooleanLazy: true, isBlueGreen:{}},
          "b_domain_docroot_changes": {presence: true, isArray: {minLength: 1}, isRealDomainHost: {wh_id:wh_id}},
          "b_repo_path1": {presence: true, isPath: {minPathLevels: 2}},
          "b_repo_path2": {isPath: {minPathLevels: 2,acceptEmptyString:true}, notTheSame: "b_repo_path1", default: {value:""}}, // default value for sqlite constraint
          "b_docroot_subdir": {isPath: {lazy:true}, default: "/"},
          "dont_clone": {isBooleanLazy: true},
        }
      validators.b_blue_green.isBlueGreen.validators = validators

      return validators

    }

    const cmd_wrapper = app.config.get("cmd_wrapper")

    const moment = require('MonsterMoment');

    const MError = require('MonsterError')

    var re = {}

    re.Insert = function(in_server_data, req){

      var d
      var dont_clone

      var task = "ok"
      var branch_id

      var v = getValidators(git.GetWebhostingId())

      return vali.async(in_server_data, v)
      	.then(ad=>{


           dont_clone = ad.dont_clone

           d = ad
           d.b_webhosting = git.GetWebhostingId()

           delete d.dont_clone

           return git.getGitInfoForWebhosting()

        })
        .then((info)=>{
           if(!info.insert_allowed) throw new MError("GIT_QUOTA_EXCEEDED")

            return knex.select().from("branches")
               .whereRaw("b_git_id=? AND b_branch=?", [git.g_id, d.b_branch])
               .then(rows=>{
                   if(rows.length > 0) throw new MError("BRANCH_ALREADY_EXISTS")

                   return Promise.resolve()
               })
        })
        .then(()=>{

           d.b_git_id = git.g_id

           return knex
             .insert(d)
             .into("branches")
             .then(ids=>{
                branch_id = ids[0]

                return Promise.resolve()
             })
             .catch(ex=>{
                if(ex.code == "SQLITE_CONSTRAINT")
                   throw new MError("NAME_ALREADY_EXISTS")

                throw ex
             })


        })
        .then(()=>{
            return git.AdjustBranches(1)

        })
        .then(()=>{
           app.InsertEvent(req, {
		     e_user_id: git.g_user_id,
		     e_event_type: "branch-create", e_other: {b_id: branch_id, b_branch: d.b_branch, b_git_id: d.b_git_id}
		   })

           return re.GetBranchById(branch_id)

        })
        .then(branch=>{
           return (!dont_clone ? branch.CloneBoth() : Promise.resolve(task))

        })


	}

  function getBranchesCommon(filters) {
    var whereExpressions = ["b_git_id=?"]
    var whereParams = [git.g_id]


    Object.keys(filters||{}).forEach(n=>{
       if(Array("b_id", "b_branch").indexOf(n) > -1) {
            whereExpressions.push(n+"=?")
            whereParams.push(filters[n])
       }
       else
       switch(n){
        /*
          case "wh_id":
            whereExpressions.push()
            break;
            */
       }
    })

    var whereStr = whereExpressions.join(" AND ")

    return {whereStr: whereStr, whereParams: whereParams}

  }
  re.GetBranchesByNameOrCompleteness = function(branch_name) {

    return getBranchesProm(
              knex.select().from("branches")
                .whereRaw("b_git_id=? AND ((b_branch=?) OR (b_pull_single_branch_only=0))", [git.g_id, branch_name])
    )

  }

  function getBranchesProm(prom){
       return prom.then(rows=>{
           return branchRowsToPromise(rows)
       })

  }

  re.GetBranches = function(filters) {

     var w = getBranchesCommon(filters)

    return getBranchesProm(
              knex.select().from("branches")
                .whereRaw(w.whereStr, w.whereParams)
    )
  }

  function GetBranchBy(prom) {
    return prom.then(rows=>{
           var branch = Common.EnsureSingleRowReturned(rows, "BRANCH_NOT_FOUND")

           return Promise.resolve(branch)
    })

  }

  re.GetBranchByName = function(name) {
     return GetBranchBy(re.GetBranches({b_branch: name}))
  }

  re.GetBranchById = function(id) {
     return GetBranchBy(re.GetBranches({b_id: id}))
  }


  re.DeleteAllBranchesOfGit = function(req) {
     return knex("branches")
       .whereRaw("b_git_id=?", [git.g_id])
       .del()
       .then(()=>{
           app.InsertEvent(req, {e_event_type: "branch-remove", e_other: {b_git_id: git.g_id}})

           return Promise.resolve()
        })

  }


    function branchRowToPromise(branch) {


            branch.b_domain_docroot_changes = lib.DomainHostStringsToHash(Common.StringToList(branch.b_domain_docroot_changes))


            branch.do_setdocroot = function(params){


              var toUpdate = {"b_current_repo_index": 0}
              var fullDocroot = params.path+branch.b_docroot_subdir
              var ps = []
              var docroot = app.GetDocroot()
              branch.b_domain_docroot_changes.forEach(h=>{

                 ps.push(docroot.putAsync("/s/[this]/docrootapi/docroots/"+wh_id+"/"+h.domain+"/entries", {host: h.host, docroot: fullDocroot}))
              })
              return Promise.all(ps)
                .then(()=>{
                   if(params.path == branch.b_repo_path1) toUpdate.b_current_repo_index = 1
                   if(params.path == branch.b_repo_path2) toUpdate.b_current_repo_index = 2

                   return knex.update(toUpdate).into("branches").whereRaw("b_id=?",[branch.b_id])

                })
                .then(()=>{
                   branch.b_current_repo_index = toUpdate.b_current_repo_index
                   return Promise.resolve(toUpdate)
                })

            }

            branch.do_log = function(params){
              //git log -5 --pretty=format:"%h - %an, %ar : %s"

               var cmd = wrap_git_cmd(params.path, ["log", "-5", '--pretty=format:"%h - %an, %ar : %s"'])
               return git.runTask({chain: cmd})
            }

            // this function returns the repo path where the pull command should be executed
            // in blue greed mode, it returns the repo which is not currently marked as in use
            function getPullPath(){
               var path = branch.b_repo_path1
               if((branch.b_repo_path2) && (branch.b_current_repo_index == 1)) {
                  path = branch.b_repo_path2
               }

               return path
            }

            branch.pull_has_finished_successfully = function(){
               // we do not touch the docroots of  non-blue-green branches
               if(!branch.b_blue_green) return

               var p = getPullPath()
               return branch.do_setdocroot({path:p})
            }

            branch.get_cmd_for_pull = function(path){
               if(!path) path = getPullPath()
               var cmd = wrap_git_cmd(path, ["pull"])
               return cmd
            }

            branch.do_pull = function(params){
               //git pull
               var cmd = branch.get_cmd_for_pull(params.path)
               return git.runTask({chain: cmd})
            }

            branch.do_revert = function(params){
              //git reset --hard c14809fa
               return vali.async(params, {commit:{presence: true, isString:{hex:true}}})
                 .then(d=>{
                     var cmd = wrap_git_cmd(params.path, ["reset", "--hard", d.commit])
                     return git.runTask({chain: cmd})
                 })

            }

            branch.do_checkout = function(params){
              //git checkout -- .

               var cmd = wrap_git_cmd(params.path, ["checkout", "--", "."])
               return git.runTask({chain: cmd})
            }

            branch.do_clean = function(params){
              //git clean -df

               var cmd = wrap_git_cmd(params.path, ["clean", "-df"])
               return git.runTask({chain: cmd})
            }

            branch.do_clone = function(params){

               var cmd = getGitCloneCmdForPath(params.path)
               return git.runTask({chain: cmd})
            }

            Array("setdocroot","log","pull","revert","checkout","clean","clone").forEach(q=>{
               git[q] = function(params){
                  return vali.async(params, {path: {presence:true, branchHasThePath: {branch: branch}}})
                    .then(()=>{
                         return branch["do_"+q](params)
                    })

               }
            })



            function getGitCloneCmdForPath(path) {
              //git clone -b <branch> <remote_repo>
              //git clone -b my-branch git@github.com:user/myproject.git

               var args = ["clone", "-b", branch.b_branch]
               if(branch.b_pull_single_branch_only)
                 args.push("--single-branch")
               args.push(git.g_remote_origin)
               args.push(".")
               var cmd = wrap_git_cmd(path, args)
               return cmd
            }
            function wrap_git_cmd(storageLocalPath, args) {
               return wrap_cmd(storageLocalPath, ["git"].concat(args))
            }
            function wrap_cmd(storageLocalPath, args) {
               var dirs = git.getAssociatedPathes()
               var wh = git.GetWebhosting()
               return {executable: cmd_wrapper, args: [wh.wh_id, wh.extras.web_path, storageLocalPath].concat(args), spawnOptions:{env: {"HOME": dirs.homedir}}}
            }




            branch.CloneBoth = function(){

               var chain = []
               chain.push(getGitCloneCmdForPath(branch.b_repo_path1))
               if(branch.b_repo_path2)
                  chain.push(getGitCloneCmdForPath(branch.b_repo_path2))

               return git.runTask({chain: chain})
                 .then(h=>{

                     h.executionPromise.then(()=>{
                        return branch.do_setdocroot({path: branch.b_repo_path1})
                     })

                     return Promise.resolve(h)
                 })

            }

            branch.Cleanup = function() {
                return branch
            }

            branch.Delete = function(req){

                return knex("branches")
                 .whereRaw("b_id=?", [branch.b_id])
                 .del()
                 .then(()=>{
                    app.InsertEvent(req, {e_event_type: "branch-remove", e_other: {g_id: git.g_id, b_id: branch.b_id, b_branch: branch.b_branch}})

                    // TODO: rmdir recursively?...

                    return git.AdjustBranches(-1)
                  })

            }

            branch.Change = function(in_data){
                var v = getValidators(git.GetWebhostingId())

                Object.keys(v).forEach(q=>{
                  if(Array("b_domain_docroot_changes", "b_docroot_subdir").indexOf(q) < 0) {
                     delete v[q]
                     return
                  }
                  delete v[q].presence
                  delete v[q].default
                })

                var d
                return vali.async(in_data, v)
                  .then(ad=>{
                     d = ad
                     d["updated_at"] = moment.now()

                     return knex.update(d).into("branches").whereRaw("b_id=?",[branch.b_id])

                  })

            }

            return branch

    }

    return re


    function branchRowsToPromise(rows, wh) {

        rows.forEach(git=>{
           branchRowToPromise(git, wh)
        })

        Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }

    function validateDocroots(authentic, userInput) {
      var re = true
      userInput.some(ui=>{
         if(!re) return

         var are = false
         authentic.some(a =>{
            if(!re) return
            if(are) return true

            if((a.raw == ui.raw)){
               are = true
               return true
            }
         })
         if(!are) {
            re = false
            return true
         }
      })
      return re
    }


}

lib.DomainHostStringsToHash = function(arr) {
  var re = []
  arr.forEach(raw=>{
    var r;
    if(typeof raw == "string") {
        r = {}
        r.raw = raw
        var res = raw.split("/");
        if(res.length != 2) throw new Error("Invalid syntax: "+raw)
        r.domain = res[0]
        r.host = res[1]

    }
    else
      r = raw;

    re.push(r)
  })

  return re
}


lib.HashToDomainHostStrings = function(arr) {
  var re = []
  arr.forEach(h=>{
    re.push(h.raw)
  })

  return " "+re.join(" ")+" "
}
