var ValidatorsLib = require("MonsterValidators")
var Common = ValidatorsLib.array()
var lib = module.exports = function(wh, knex, app) {

    const dotq = require("MonsterDotq")
    const fs = dotq.fs()
    const path = require("path")

    const TokenLib = require("Token")

    const default_ssh_settings = {
      StrictHostKeyChecking: false,
    }

    var ssh_validator = {
       StrictHostKeyChecking: {isBooleanLazy: true}
    }


    const BranchLib = require("models/branch.js")

  	var vali = ValidatorsLib.ValidateJs()

    var activeTasksOfWebhosting = {}

     vali.validators.isGitRemote = function(value, options, key, attributes) {
         const isGitUrl = require("is-git-url")
         if(!value) return
         if(!isGitUrl(value))
            return "not a git url"
      }

    function getValidators() {
      var validators = {
          "g_id": {isString: {strictName: true}},
          "g_remote_origin": {presence: true, isString: true, isGitRemote: true},
          "g_name": {presence: true, isString: {ascii:true}},
        }

      return validators

    }

    const this_server_hostname = app.config.get("mc_server_name") +"."+ app.config.get("mc_primary_domain")

    const ssh_deployment_keys_directory = app.config.get("ssh_deployment_keys_directory")
    const rsa_private_key_bitlength = app.config.get("rsa_private_key_bitlength")

    const moment = require('MonsterMoment');

    const MError = require('MonsterError')

    var re = {}

    re.Insert = function(in_server_data, req){

      var d

      var v = getValidators()

      return vali.async(in_server_data, v)
      	.then(ad=>{

           d = ad

           d.g_webhosting = wh.wh_id
           d.g_user_id = wh.wh_user_id

           if(!d.g_id)
              return TokenLib.GetTokenAsync(16, "hex")

           return d.g_id;
        })
        .then(token=>{
           d.g_id = token

           return re.GetGitInfoForWebhosting()

        })
        .then((info)=>{
           if(!info.insert_allowed) throw new MError("GIT_QUOTA_EXCEEDED")


           return knex
             .insert(d)
             .into("gits")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                if(ex.code == "SQLITE_CONSTRAINT")
                   throw new MError("NAME_ALREADY_EXISTS")

                throw ex
             })


        })
        .then((id)=>{
           app.InsertEvent(req, {
             e_user_id: d.g_user_id,
		     e_event_type: "git-create", e_other: {g_id: id, g_name: d.g_name, g_user_id: d.g_user_id, g_remote_origin: d.g_remote_origin}
		   })

           return Promise.resolve({g_id: d.g_id})

        })


	}

  function getGitsOfWebhostingCommon(filters) {
    var whereExpressions = ["g_webhosting=?"]
    var whereParams = [wh.wh_id]

    Object.keys(filters||{}).forEach(n=>{
       if(Array("g_id", "g_name", "g_user_id").indexOf(n) > -1) {
            whereExpressions.push(n+"=?")
            whereParams.push(filters[n])
       }
       else
       switch(n){
        /*
          case "wh_id":
            whereExpressions.push()
            break;
            */
       }
    })

    var whereStr = whereExpressions.join(" AND ")

    return {whereStr: whereStr, whereParams: whereParams}

  }

  re.GetAllGitsOfWebhosting = function(filters) {

     var w = getGitsOfWebhostingCommon(filters)


    return knex.select().from("gits")
       .whereRaw(w.whereStr, w.whereParams)
       .then(rows=>{
           return gitRowsToPromise(rows)
       })

  }

  function GetGitBy(p){
     return p.then(rows=>{
           var git = Common.EnsureSingleRowReturned(rows, "GIT_NOT_FOUND")

           return Promise.resolve(git)
       })

  }
  re.GetGitBy_id = function(g_id) {

     return GetGitBy(re.GetAllGitsOfWebhosting({g_id: g_id}))
  }
  re.GetGitBy_name = function(g_name) {

     return GetGitBy(re.GetAllGitsOfWebhosting({g_name: g_name}))
  }
  re.RestoreSshFiles = function(files){
      var pathes = getAssociatedPathes()
      return createDirectoryStructure(pathes)
        .then(()=>{
           return createPasswdFile(pathes);
        })
        .then(()=>{
            return dotq.linearMap({array: Object.keys(files), action: file=>{
                if(file.match(/\.\./)) throw new MError("INVALID_FILENAME");
                return fs.writeFileAsync(path.join(pathes.sshdir, file), files[file])
            }})

        })
  }

  re.ReadSshFiles = function(){
      var result = {};
      var pathes = getAssociatedPathes()
      return fs.readdirAsync(pathes.sshdir)
        .then(files=>{
           return dotq.linearMap({array: files, action: file=>{
              return fs.readFileAsync(path.join(pathes.sshdir, file), "utf8")
                .then((content)=>{
                    result[file] = content;
                })
           }})
        })
        .then(()=>{
           return result;
        })
  }

  re.GetNumberOfGitsOfWebhosting = function(){
     var w = getGitsOfWebhostingCommon()

    var stats = { current_repositories: 0, current_branches: 0}
    return knex("gits").count("* AS c")
       .whereRaw(w.whereStr, w.whereParams)
       .then(rows=>{
           stats.current_repositories = rows[0].c

           return knex("gits").sum("g_branches AS c")
             .whereRaw(w.whereStr, w.whereParams)
       })
       .then(rows=>{
           stats.current_branches = rows[0].c || 0 // it might be null initially when there are no git rows


           return Promise.resolve(stats)
       })

  }

  re.GetGitInfoForWebhosting = function() {
       var vre
       return re.GetNumberOfGitsOfWebhosting()
         .then(stats=>{
             vre = {current_repositiories: stats.current_repositories, current_branches: stats.current_branches, t_git_max_repos: wh.template.t_git_max_repos, t_git_max_branches: wh.template.t_git_max_branches}
             vre.insert_allowed = vre.current_repositiories < vre.t_git_max_repos && vre.current_branches < vre.t_git_max_branches

             var pathes = getAssociatedPathes()
             return getSshConfig(pathes)
         })
         .then(ssh=>{
             vre.ssh = ssh
             return Promise.resolve(vre)
         })

  }

  re.DeleteAllGitsOfWebhosting = function(req) {
     return knex("branches")
       .whereRaw("b_webhosting=?", [wh.wh_id])
       .del()
       .then(()=>{
           return knex("gits")
             .whereRaw("g_webhosting=?", [wh.wh_id])
             .del()

       })
       .then(()=>{
           app.InsertEvent(req, {
		     e_event_type: "git-remove", e_other: {g_webhosting: wh.wh_id}
		   })

           delete activeTasksOfWebhosting[wh.wh_id]

           return Promise.resolve()
        })

  }

  re.DeleteFilesAssociatedWithWebhosting = function() {
     const del = require("MonsterDel")
     var pathes = getAssociatedPathes()
     // console.log("hey",pathes)
     return del(path.join(pathes.homedir,"**"),{"force": true}) // these files are from unit testing
     /*
       .then((x)=>{
         console.log("deleted",x)
         return Promise.resolve()
       })
     */
  }

 re.GetActiveTasksOfWebhosting = function(){
    return activeTasksOfWebhosting[wh.wh_id] || []
 }

 function getAssociatedPathes() {
    var homedir = path.join(ssh_deployment_keys_directory, ""+wh.wh_id)
    var sshdir = path.join(homedir, ".ssh")
    return {
      homedir: homedir,
      sshdir: sshdir,
      ssh_known_hosts: path.join(sshdir, "known_hosts"),
      ssh_config_file: path.join(sshdir, "config"),
      passwd_file: path.join(homedir, "passwd"),
      ssh_public_key: path.join(sshdir, "id_rsa.pub"),
      ssh_private_key: path.join(sshdir, "id_rsa"),
    }
 }

 function createPasswdFile(pathes) {
    return fs.writeFileAsync(pathes.passwd_file, `u${wh.wh_id}:x:${wh.wh_id}:${wh.wh_id}:,,,:${pathes.homedir}:/bin/bash\n`)
 }

 function createDirectoryStructure(pathes){
      const mkdirp = dotq.single(require("mkdirp"))
      return mkdirp(pathes.sshdir)
        .catch(ex=>{})

 }

 function createSshConfig(pathes, settings) {

    var str = "#"+JSON.stringify(settings)+"\n"
    str += "StrictHostKeyChecking "+(settings.StrictHostKeyChecking ? "yes" : "false")+"\n"

    return createDirectoryStructure(pathes)
       .then(()=>{
         return fs.writeFileAsync(pathes.ssh_config_file, str)
       })
 }

 function getSshConfig(pathes, skipAutoCreate) {

   const firstLine = require("FirstLineAsJSON")

    return firstLine.ReadAsync(pathes.ssh_config_file, function(line, originalParser){

             var recs = originalParser(line)

             if(!vali.isObject(recs))
                throw new MError("INVALID_JSON_IN_SSH_CONFIG_FILE")

             return recs
          }).catch(ex=>{
            if(skipAutoCreate) return Promise.resolve({})

            return createSshConfig(pathes, default_ssh_settings)
              .then(()=>{
                  return getSshConfig(pathes, true)
              })
          })
 }

 re.ChangeSshConfig = function(params) {
    return vali.async(params, ssh_validator)
      .then((d)=>{
          var pathes = getAssociatedPathes()
          return createSshConfig(pathes, d)
      })
 }

 re.GetSshDeploymentKey = function(params){
   var pathes
   return vali.async(params, {regenerate:{isBooleanLazy: true}})
     .then(d=>{
        pathes = getAssociatedPathes()
        if(d.regenerate) return Promise.resolve()

        return fs.statAsync(pathes.ssh_public_key).catch(x=>{ return Promise.resolve() })
     })
     .then(s=>{
         if((!s)||(!s.isFile)||(!s.isFile())) {
            // need to generate a new one

            return createDirectoryStructure(pathes)
              .then(()=>{
                  return createPasswdFile(pathes)
              })
              .then(()=>{
                  return createSshConfig(pathes, default_ssh_settings)
              })
              .then(()=>{
                  return app.commander.spawn({chain:{executable:"ssh-keygen", args:["-t", "rsa", "-N", "","-b", rsa_private_key_bitlength, "-f", pathes.ssh_private_key, "-C", wh.wh_id+"@"+this_server_hostname]}})
              })
              .then(h=>{
                  return h.executionPromise
              })
              .then(()=>{
                  return fs.statAsync(pathes.ssh_known_hosts)
                    .catch(x=>{
                        return fs.writeFileAsync(pathes.ssh_known_hosts, "")
                    })
              })
              .then(()=>{
                  var ps = []

                  Array("ssh_public_key","ssh_private_key","ssh_known_hosts").forEach(k=>{ // we do not chown sshdir here
                      var p = pathes[k]
                      var prom = fs.chownAsync(p, wh.wh_id, wh.wh_id).catch(x=>{})
                      ps.push(prom)
                  })
                  return Promise.all(ps)

              })

         }

         return Promise.resolve()
     })
     .then(()=>{
         return fs.readFileAsync(pathes.ssh_public_key, {encoding: "utf8"})
     })

 }

 re.RowToPromise = function(row){
    return gitRowToPromise(row)
 }


    function gitRowToPromise(git) {

      if(!activeTasksOfWebhosting[wh.wh_id]) activeTasksOfWebhosting[wh.wh_id] = []



           var branch

           git.webhook_url = "https://"+app.config.get("mc_primary_domain")+"/api/s/"+app.config.get("mc_server_name")+"/git/triggers/"+wh.wh_id+"/"+git.g_id

           git.getAssociatedPathes = function(){
              return getAssociatedPathes()
           }

           git.AdjustBranches = function(offset) {
               return knex.raw('UPDATE gits SET g_branches=g_branches+? WHERE g_id = ?', [offset, git.g_id]).then(()=>{return Promise.resolve()})
           }

           git.InsertBranch = function(params, req) {
              return branch.Insert(params, req)
           }

           git.GetWebhostingId = function(){
              return wh.wh_id
           }
           git.GetWebhosting = function(){
              return wh
           }

            git.GetBranchByName = function(name) {
                return branch.GetBranchByName(name)
            }
             git.GetBranches = function(){
                return branch.GetBranches()
             }


            git.Pull = function(r){

                  if(r.zen) return Promise.resolve("ok")

                  var branch_only = lib.GetBranchFiltersBasedOnWebhook(r)
                  if(branch_only === false) return Promise.reject(new MError("NOT_A_PUSH_EVENT"))

                  var branches
                  return branch.GetBranchesByNameOrCompleteness(branch_only)
                    .then(aBranches=>{
                       branches = aBranches
                       if(!branches.length) throw new MError("NO_BRANCH_FOUND")

                       var chain = []
                       branches.forEach(branch=>{
                          chain.push(branch.get_cmd_for_pull())
                       })


                       return git.runTask({chain: chain})

                    })
                    .then(h=>{
                       h.executionPromise.then(()=>{
                          // pulls have successfully completed.
                          // we have some more tasks to do now, like docroot change and b_branch_index change
                           branches.forEach(branch=>{
                              branch.pull_has_finished_successfully()
                           })


                       })

                       return Promise.resolve(h)
                    })

            }


            git.getGitInfoForWebhosting = function() {
               return re.GetGitInfoForWebhosting()
            }


            git.taskCallback = function(h){
                activeTasksOfWebhosting[wh.wh_id].push(h.id)

                h.executionPromise
                  .catch(aex=>{})
                  .then(()=>{
                    removeTaskFromCache(h.id)

                  })

                return Promise.resolve(h)
            }




            git.Cleanup = function() {
                 return git
            }

            git.Delete = function(req){

                return knex("gits")
                 .whereRaw("g_id=?", [git.g_id])
                 .del()
                 .then(()=>{
                    app.InsertEvent(req, {
                      e_user_id: git.g_user_id,
					  e_event_type: "git-remove", e_other: {g_id: git.g_id, g_name: git.g_name, g_user_id: git.g_user_id, g_remote_origin: git.g_remote_origin}
					})

                    // TODO: rmdir recursively?...

                    return branch.DeleteAllBranchesOfGit(req)
                  })

            }

            git.Change = function(in_data){
                var v = getValidators()

                Object.keys(v).forEach(q=>{
                  if(Array("g_name").indexOf(q) < 0) {
                     delete v[q]
                     return
                  }
                  delete v[q].presence
                  delete v[q].default
                })

                var d
                return vali.async(in_data, v)
                  .then(ad=>{
                     d = ad
                     d["updated_at"] = moment.now()

                     return knex.update(d).into("gits").whereRaw("g_id=?",[git.g_id])

                  })

            }

            git.runTask=function(taskOptions){
               if(!taskOptions.triggerCallbacks) taskOptions.triggerCallbacks = []
               taskOptions.triggerCallbacks.push(git.taskCallback)

               return app.commander.spawn(taskOptions)
                 .then(git.taskCallback)
            }


            branch  = BranchLib(git, knex, app)


            return git



            function removeTaskFromCache(task_id) {

                    var index = activeTasksOfWebhosting[wh.wh_id].indexOf(task_id);
                    if(index > -1)
                      activeTasksOfWebhosting[wh.wh_id].splice(index, 1);

            }

    }

    return re


    function gitRowsToPromise(rows) {

        rows.forEach(git=>{
           gitRowToPromise(git)
        })

        Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }



}


lib.GetBranchFiltersBasedOnWebhook = function(p) {
    if(typeof p != "object") return false

    var b_branch = false

    if(p.ref)
    {
       var r = /^refs\/heads\/(.+)$/
       var m = r.exec(p.ref)
       if(m)
          b_branch = m[1]

    }

    if((p.push)&&(p.push.changes)&&(p.push.changes.length)&&(p.push.changes[0].new)&&(p.push.changes[0].new.name))
    {
       if(typeof p.push.changes[0].new.name == "string")
         b_branch = p.push.changes[0].new.name
    }


    return b_branch

}

lib.GetAllGits = function(knex, app){

    return knex.select().from("gits")
     .then(rows=>{
         var re = []
         rows.forEach(row=>{
            var wh = {wh_id: row.g_webhosting}
            var gitlib = lib(wh, knex, app)
            re.push(gitlib.RowToPromise(row))
         })
         Common.AddCleanupSupportForArray(re)

         return Promise.resolve(re.Cleanup())
     })
}

