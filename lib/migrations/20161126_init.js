
exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('gits', function(table) {
            table.string('g_id').primary();

            // these fields here cannot be changed after the entry was added
            table.uuid('g_user_id').notNullable();
            table.integer('g_webhosting').notNullable();

            table.string('g_remote_origin').notNullable();

            // these two can be
            table.string('g_name').notNullable().unique();

            table.integer("g_branches").notNullable().defaultTo(0);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),

         knex.schema.createTable('branches', function(table) {
            table.increments('b_id').primary();


            table.integer('b_webhosting').notNullable();

            // these fields here cannot be changed after the entry was added
            table.uuid('b_git_id')
                 .notNullable()
                 .references('g_id')
                 .inTable('gits');            

            table.string('b_branch').notNullable().defaultTo("");

            table.boolean('b_pull_single_branch_only').notNullable().defaultTo(true);

            table.boolean('b_blue_green').notNullable().defaultTo(false);
            table.string('b_repo_path1').notNullable();
            table.string('b_repo_path2').notNullable();
            table.string('b_docroot_subdir').notNullable();
            
            table.integer('b_current_repo_index').notNullable().defaultTo(0);

            table.string('b_domain_docroot_changes').notNullable(); // something like: " domain.hu/host1.domain.hu domain.hu/host2.domain.hu "
            // will be returned as an array of hashes

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),

    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('gits'),
        knex.schema.dropTable('branches'),
    ])
	
};
