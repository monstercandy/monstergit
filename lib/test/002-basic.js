require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../git-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const blueGreen = {
        1: "blue",
        2: "green",
    }

    const fs = require("MonsterDotq").fs()
    const webhosting = {
        wh_id: 12345, wh_user_id: 23456, template:{t_git_max_repos: 3, t_git_max_branches: 9}, extras: { web_path: "/full/path/to/storage" }
    }
    const simpleWebHostingInfoObject = {
                GetInfo: function(wh_id) {
                   return Promise.resolve(webhosting)
                }
     }

     const standardMockedGetDocroot = function(){
                 return {
                    getAsync: function(path){
                        assert.equal(path, "/s/[this]/docrootapi/docroots/hostentries/"+webhosting.wh_id)
                        return Promise.resolve({result:
                            [
                             {raw: "domain1.hu/www.domain1.hu", domain: "domain1.hu", host: "www.domain1.hu"},
                             {raw: "domain2.hu/www.domain2.hu", domain: "domain2.hu", host: "www.domain2.hu"},
                            ]
                        })
                    }
                 }
             }

    const branch_should_look_like = { b_id: 1,
    b_webhosting: 12345,
    b_git_id: 'TODO',
    b_branch: 'dev',
    b_pull_single_branch_only: 0,
    b_blue_green: 1,
    b_repo_path1: '/foo/dev/blue/',
    b_repo_path2: '/foo/dev/green/',
    b_docroot_subdir: '/somebody/',
    b_current_repo_index: 2,
    b_domain_docroot_changes: [ {} ],
     }

    const updated_branch_should_look_like = { b_id: 1,
    b_webhosting: 12345,
    b_git_id: 'TODO',
    b_branch: 'dev',
    b_pull_single_branch_only: 0,
    b_blue_green: 1,
    b_repo_path1: '/foo/dev/blue/',
    b_repo_path2: '/foo/dev/green/',
    b_docroot_subdir: '/somebody/',
    b_current_repo_index: 2,
    b_domain_docroot_changes: [
      {
      "domain": "domain2.hu",
      "host": "www.domain2.hu",
      "raw": "domain2.hu/www.domain2.hu",
      }
     ],
     }

    const database_should_look_like = { g_id: "todo",
  g_user_id: '23456',
  g_webhosting: 12345,
  g_branches: 0,
  g_remote_origin: 'https://github.com/irsl/dfwfw.git',
  g_name: 'myname',
  webhook_url: "https://some.primary.domain/api/s/s123/git/triggers/12345/",
    }

    const updated_database_should_look_like = { g_id: "todo",
  g_user_id: '23456',
  g_webhosting: 12345,
  g_branches: 0,
  g_remote_origin: 'https://github.com/irsl/dfwfw.git',
  g_name: 'new_name',
  webhook_url: "https://some.primary.domain/api/s/s123/git/triggers/12345/",
   }

    const branch_good_config = {dont_clone: true, b_pull_single_branch_only:false, b_domain_docroot_changes: ["domain1.hu/www.domain1.hu"], b_blue_green: true, b_repo_path1: "/foo/dev/blue", b_repo_path2: "/foo/dev/green", b_branch: "dev" }
    const git_good_config = {g_name:"myname", g_remote_origin: "https://github.com/irsl/dfwfw.git", }

    const origMonsterInfoWebhosting = app.MonsterInfoWebhosting
    const origGetDocroot = app.GetDocroot
    const origCommander = app.commander

    var g_id

    describe('git related tests', function() {



        it('get ssh key', function(done) {

             const rsapubContent = "pub-content"

             app.MonsterInfoWebhosting = simpleWebHostingInfoObject
             app.commander = {
                spawn: function(params){
                     console.log("spawn", params)
                    const rsapriv = 'ssh-keys.d\\12345\\.ssh\\id_rsa'

                    assert.deepEqual({chain: { executable: 'ssh-keygen',
     args:
      [ '-t',
        'rsa',
        '-N',
        '',
        '-b',
        2048,
        '-f',
        rsapriv,
        '-C',
        '12345@s123.some.primary.domain' ] }}, params)
                    return fs.writeFileAsync(rsapriv+".pub", rsapubContent)
                      .then(()=>{
                          return Promise.resolve({id:123, executionPromise: Promise.resolve({output:"foo"})})

                      })
                }
             }

             mapi.post( "/12345/ssh_deployment_key", {}, function(err, result, httpResponse){
                assert.equal(result, rsapubContent)

                app.commander = origCommander

                done()

             })

        })

        it('get info, insert_allowed should be false', function(done) {

            app.MonsterInfoWebhosting = {
                GetInfo: function(wh_id) {
                   return Promise.resolve({wh_id: 12345, template:{t_git_max_repos: 0}})
                }
            }

             mapi.get( "/12345/info",function(err, result, httpResponse){
                assert.isNull(err)

                assert.propertyVal(result, "current_repositiories", 0)
                assert.propertyVal(result, "t_git_max_repos", 0)
                assert.propertyVal(result, "insert_allowed", false)

                done()

             })

        })

        gitListShouldBeEmpty()

        it('get git by id should be rejected', function(done) {

             mapi.get( "/12345/id/12",function(err, result, httpResponse){
                assert.propertyVal(err, "message", "GIT_NOT_FOUND")

                done()

             })

        })

        it('get info, insert_allowed should be true', function(done) {

             mapi.get( "/12345/info",function(err, result, httpResponse){
                assert.isNull(err)

                assert.propertyVal(result, "current_repositiories", 0)
                assert.propertyVal(result, "t_git_max_repos", 3)
                assert.propertyVal(result, "insert_allowed", true)

                assert.property(result, "ssh")

                assert.propertyVal(result.ssh, "StrictHostKeyChecking", false)

                done()

             })

        })

        it("change ssh strict to true",function(done){
             mapi.post("/12345/ssh",{StrictHostKeyChecking:true},function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(result,"ok")
                done()
             })
        })

        it('get info, StrictHostKeyChecking should be true', function(done) {

             mapi.get( "/12345/info",function(err, result, httpResponse){
                assert.isNull(err)

                assert.property(result, "ssh")

                assert.propertyVal(result.ssh, "StrictHostKeyChecking", true)

                done()

             })

        })


        it('trying to insert a git to an expired webhosting, should be rejected', function(done) {

             app.MonsterInfoWebhosting = {
                        GetInfo: function(wh_id) {
                           return Promise.resolve({wh_id:12345,wh_is_expired:true})
                        }
             }


             mapi.put( "/12345", git_good_config, function(err, result, httpResponse){
                assert.propertyVal(err,"message","WEBHOSTING_IS_EXPIRED")
                app.MonsterInfoWebhosting = simpleWebHostingInfoObject

                done()

             })

        })


        it('insert git, should work', function(done) {

             mapi.put( "/12345", git_good_config, function(err, result, httpResponse){
                assert.property(result, "g_id")

                g_id = result.g_id
                database_should_look_like.g_id = g_id
                database_should_look_like.webhook_url += g_id
                updated_database_should_look_like.webhook_url += g_id
                updated_database_should_look_like.g_id = g_id
                branch_should_look_like.b_git_id = g_id
                updated_branch_should_look_like.b_git_id = g_id

                done()

             })

        })

        it('insert git, should be rejected because of same name', function(done) {

             mapi.put( "/12345", git_good_config, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "NAME_ALREADY_EXISTS")

                app.GetDocroot = origGetDocroot

                done()

             })

        })

        it('get git by id should be ok', function(done) {

             mapi.get( "/12345/id/"+g_id,function(err, result, httpResponse){
                assert.isNull(err)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, database_should_look_like)

                done()

             })

        })


        it('get git by name should be ok', function(done) {

             mapi.get( "/12345/name/myname",function(err, result, httpResponse){
                assert.isNull(err)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, database_should_look_like)

                done()

             })

        })

        it('get all gits should return this row', function(done) {

             mapi.get( "/12345", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                delete result[0].created_at
                delete result[0].updated_at


                assert.deepEqual(result, [database_should_look_like])

                done()

             })

        })





        it('changing some parameters of the git', function(done) {

             mapi.post( "/12345/id/"+g_id, {"g_name":"new_name"}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()

             })

        })


        it('get git by new name should be ok', function(done) {

             mapi.get( "/12345/name/new_name",function(err, result, httpResponse){
                assert.isNull(err)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, updated_database_should_look_like)

                done()

             })

        })

    })



   describe("branch related tests", function(){


        branchesShouldBeEmpty()

        it('insert branch without g_blue_green, g_docroot2 should be present in the response', function(done) {

             mapi.put( "/12345/name/new_name/branches", {b_blue_green: true, }, function(err, result, httpResponse){

                assert.property(err, "errorParameters")
                assert.property(err.errorParameters, "b_repo_path1")
                assert.property(err.errorParameters, "b_repo_path2")

                done()

             })

        })

        it('insert branch without g_blue_green, g_docroot2 should not be present in the response', function(done) {

             mapi.put( "/12345/name/new_name/branches", {b_blue_green: false}, function(err, result, httpResponse){
                assert.property(err, "errorParameters")
                assert.property(err.errorParameters, "b_repo_path1")
                assert.notProperty(err.errorParameters, "b_repo_path2")

                done()

             })

        })

        it('insert branch, invalid domain/host specified', function(done) {

             app.GetDocroot = standardMockedGetDocroot

             mapi.put( "/12345/name/new_name/branches", {b_domain_docroot_changes: ["foo/bar"], b_blue_green: true, b_repo_path1: "/foo/bar1", b_repo_path2: "/foo/bar2" }, function(err, result, httpResponse){
                assert.property(err, "errorParameters")
                assert.property(err.errorParameters, "b_domain_docroot_changes")

                done()

             })

        })

        insertBranchShouldBeOk()


        it('changing some parameters of the branch', function(done) {

             mapi.post( "/12345/name/new_name/branch/dev", {"b_domain_docroot_changes":["domain2.hu/www.domain2.hu"],"b_docroot_subdir":"/somebody"}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                app.GetDocroot = origGetDocroot

                done()

             })

        })

        Array(1,2).forEach(i=>{
            it('setting docroot: '+i, function(done) {

                 app.GetDocroot = function(){
                     return {
                        putAsync: function(path, data){
                            // console.log("putAsync", path, data)
                            assert.equal("/s/[this]/docrootapi/docroots/12345/domain2.hu/entries", path)
                            assert.deepEqual({ host: 'www.domain2.hu', docroot: '/foo/dev/'+blueGreen[i]+'//somebody/' }, data)
                            return Promise.resolve({result: "ok"})
                        }
                     }
                 }

                 mapi.post( "/12345/name/new_name/branch/dev/setdocroot", {path: "/foo/dev/"+blueGreen[i]+"/"}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, {b_current_repo_index: i})

                    done()

                 })

            })

        })


        it('b_current_repo_index should match what we just set', function(done) {

             mapi.get( "/12345/name/new_name/branch/dev",function(err, result, httpResponse){
                assert.isNull(err)
                assert.propertyVal(result, "b_current_repo_index", 2)

                done()

             })

        })


        it('get branches should show the dev one', function(done) {

             mapi.get( "/12345/name/new_name/branches", function(err, result, httpResponse){
                assert.isNull(err)
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                delete result[0].created_at
                delete result[0].updated_at
                assert.deepEqual(result, [updated_branch_should_look_like])

                done()

             })
        })


        it('delete branch by name', function(done) {

             mapi.delete( "/12345/id/"+g_id+"/branch/dev", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()

             })

        })



        branchesShouldBeEmpty()

        insertBranchShouldBeOk()

            it('insert branch qa with auto cloning', function(done) {

                 var putWasCalled = 0
                 app.GetDocroot = function(){
                     return {
                        getAsync: standardMockedGetDocroot().getAsync,
                        putAsync: function(path, data){
                            putWasCalled++
                            console.log("putAsync", path, data)
                            assert.equal("/s/[this]/docrootapi/docroots/12345/domain1.hu/entries", path)
                            assert.deepEqual({ host: 'www.domain1.hu', docroot: '/foo/qa/blue//' }, data)
                            return Promise.resolve({result: "ok"})
                        }
                     }
                 }

                 app.commander = {
                    spawn: function(options) {
                        // console.log(options.chain)
                        assert.property(options, "chain")
                        // console.log(options.chain)
                        assert.deepEqual([ { executable: 'mock-cmds/wrapper.bat', args: [
                              12345,
                              '/full/path/to/storage',
                              '/foo/qa/blue/',
                              'git',
                              'clone',
                              '-b',
                              'qa',
                              '--single-branch',
                              'https://github.com/irsl/dfwfw.git',
                              '.'
                        ], spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} },
                        { executable: 'mock-cmds/wrapper.bat', args: [
                              12345,
                              '/full/path/to/storage',
                              '/foo/qa/green/',
                              'git',
                              'clone',
                              '-b',
                              'qa',
                              '--single-branch',
                              'https://github.com/irsl/dfwfw.git',
                              '.'
                        ], spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} } ], options.chain)

                        return Promise.resolve({id: 1, executionPromise: Promise.resolve()})
                    }
                 }

                 mapi.put( "/12345/name/new_name/branches",
                    {b_domain_docroot_changes: ["domain1.hu/www.domain1.hu"], b_blue_green: true, b_repo_path1: "/foo/qa/blue", b_repo_path2: "/foo/qa/green", b_branch: "qa" }
                    , function(err, result, httpResponse){
                        assert.property(result,"id")

                        assert.equal(putWasCalled, 1)

                        app.commander = origCommander
                        app.GetDocroot = origGetDocroot

                        done()

                 })

            })

        it('request a manual pull', function(done) {
                 app.commander = {
                    spawn: function(options) {
                        assert.property(options, "chain")
                        assert.deepEqual({ executable: 'mock-cmds/wrapper.bat', args: [ 12345, '/full/path/to/storage', '/foo/dev/blue/', 'git', 'pull' ], spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} }, options.chain)

                        return Promise.resolve({id: 1, executionPromise: Promise.resolve()})
                    }
                 }

                 mapi.post( "/12345/name/new_name/branch/dev/pull", {"path":"/foo/dev/blue/"}, function(err, result, httpResponse){
                        assert.property(result,"id")

                        app.commander = origCommander

                        done()

                 })

        })

        it('webhook callback with invalid payload should be rejected', function(done){

                 mapi.post( "/triggers/12345/"+g_id, {"some":"crap"}, function(err, result, httpResponse){

                        assert.isNotNull(err)
                        assert.propertyVal(err, "message", "NOT_A_PUSH_EVENT")

                        done()

                 })
        })

        it('webhook to qa instance should trigger pull of the qa branch, green path and also the dev instance since it is non-single-branch', function(done){

             app.commander = {
                spawn: function(params){
                    // console.log("spawn", params.chain)
                    assert.property(params, "chain")
                    assert.deepEqual([
                        { executable: 'mock-cmds/wrapper.bat',
    args: [ 12345, '/full/path/to/storage', '/foo/dev/blue/', 'git', 'pull' ],
    spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} },
                         { executable: 'mock-cmds/wrapper.bat', args: [ 12345, '/full/path/to/storage', '/foo/qa/green/', 'git', 'pull' ], spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} } ],
                     params.chain)
                    return Promise.resolve({id:1, executionPromise: Promise.resolve("ok")})
                }
             }

             var putWasCalled = 0
                 app.GetDocroot = function(){
                     return {
                        putAsync: function(path, data){
                            console.log("putAsync", path, data)
                            assert.equal("/s/[this]/docrootapi/docroots/12345/domain1.hu/entries", path)
                            if(putWasCalled == 0)
                              assert.deepEqual({ host: 'www.domain1.hu', docroot: '/foo/dev/blue//' }, data)
                            if(putWasCalled == 1)
                              assert.deepEqual({ host: 'www.domain1.hu', docroot: '/foo/qa/green//' }, data)
                            putWasCalled++
                            return Promise.resolve({result: "ok"})
                        }
                     }
                 }


                 mapi.post( "/triggers/12345/"+g_id, {"ref":"refs/heads/qa"}, function(err, result, httpResponse){

                        assert.isNull(err)
                        assert.property(result, "id")
                        assert.equal(putWasCalled, 2)
                        done()

                 })

        })

        it('webhook to dev instance should trigger pull of dev only (since qa is a single branch), dev should be green (was blue)', function(done){


             app.commander = {
                spawn: function(params){
                     console.log("spawn", params.chain)
                    assert.property(params, "chain")
                    assert.deepEqual([ { executable: 'mock-cmds/wrapper.bat',
    args:
     [ 12345,
       '/full/path/to/storage',
       '/foo/dev/green/',
       'git',
       'pull' ],
    spawnOptions:{env: { HOME: 'ssh-keys.d\\12345' }} } ], params.chain)
                    return Promise.resolve({id:2, executionPromise: Promise.resolve("ok")})
                }
             }

             var putWasCalled = 0
                 app.GetDocroot = function(){
                     return {
                        putAsync: function(path, data){
                            console.log("putAsync", path, data)
                            assert.equal("/s/[this]/docrootapi/docroots/12345/domain1.hu/entries", path)
                              assert.deepEqual({ host: 'www.domain1.hu', docroot: '/foo/dev/green//' }, data)
                            putWasCalled++
                            return Promise.resolve({result: "ok"})
                        }
                     }
                 }

                 mapi.post( "/triggers/12345/"+g_id, {"ref":"refs/heads/dev"}, function(err, result, httpResponse){

                        assert.isNull(err)
                        assert.property(result, "id")
                        assert.equal(putWasCalled, 1)
                        done()


                 })
        })



        it("get all gits should return all gits", function(done){


                 mapi.get( "/", function(err, result, httpResponse){

                        assert.isNull(err)
                        assert.ok(Array.isArray(result))
                        // console.log(result)
                        assert.equal(result.length, 1)

                        updated_database_should_look_like.g_branches = 2 // we have two branches already
                        delete result[0].created_at
                        delete result[0].updated_at
                        // console.log(updated_database_should_look_like)
                        assert.deepEqual(result[0], updated_database_should_look_like)
                        done()

                 })
        })



        it('delete git by id should be ok', function(done) {

             mapi.delete( "/12345/id/"+g_id, {}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()

             })

        })

        gitListShouldBeEmpty()


        it('delete webhosting completely (accossiated files should be gone)', function(done) {

             mapi.delete( "/12345/", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                fs.statAsync("ssh-keys.d\\12345")
                  .then(()=>{
                     done(new Error("dir should have been deleted"))
                  })
                  .catch(ex=>{

                        done()

                  })


             })

        })

    })


        function insertBranchShouldBeOk(){

            it('insert branch, should work', function(done) {

                 app.GetDocroot = standardMockedGetDocroot

                 mapi.put( "/12345/name/new_name/branches", branch_good_config, function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    done()

                 })

            })

        }



        function gitListShouldBeEmpty() {

            it('get all gits should be empty', function(done) {

                 app.MonsterInfoWebhosting = simpleWebHostingInfoObject

                 mapi.get( "/12345",function(err, result, httpResponse){
                    assert.deepEqual(result, [])

                    done()

                 })

            })


        }


        function branchesShouldBeEmpty(){

            it('get branches should be empty', function(done) {

                 mapi.get( "/12345/name/new_name/branches", function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, [])
                    done()
                 })
            })

        }


}, 10000)

