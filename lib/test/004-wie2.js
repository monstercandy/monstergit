require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../git-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const storage_id = 32346

    const fs = require("MonsterDotq").fs();

    var getInfoCalls = 0;
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){

                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id,
                     wh_user_id: 123,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_git_max_repos: 3,
                       t_git_max_branches: 3,
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }




     const standardMockedGetDocroot = function(){
                 return {
                    getAsync: function(path){
                        assert.equal(path, "/s/[this]/docrootapi/docroots/hostentries/"+storage_id)
                        return Promise.resolve({result:
                            [
                             {raw: "git.monstermedia.hu/test.git.monstermedia.hu", domain: "git.monstermedia.hu", host: "test.git.monstermedia.hu"},
                             {raw: "git.monstermedia.hu/git.monstermedia.hu", domain: "git.monstermedia.hu", host: "git.monstermedia.hu"},
                             {raw: "git.monstermedia.hu/www.git.monstermedia.hu", domain: "git.monstermedia.hu", host: "www.git.monstermedia.hu"},
                            ]
                        })
                    }
                 }
             }

  const restorePayload = {
  "ssh": {
    "config": "#{\"StrictHostKeyChecking\":false}\nStrictHostKeyChecking false\n",
    "id_rsa": "-----BEGIN RSA PRIVATE KEY-----\n-----END RSA PRIVATE KEY-----\n",
    "id_rsa.pub": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCtTg83S1JWQAk9j7OeEvJ2QpqeQDK6ZIMJZUIuoeLyqTWIvuZ7Q6xv99bz+TIBMwr4Klyk7BB+/3bHfgifp2LiUZZIYoPbYOLo6qEOvQOzDpfR+YmiT93c4vHr8x67OF3W7UaSsh2dY8eDu3A2lGX0L+IGzk/40AUctDymGqvBTxCrAp+HqsTMdJq8Gi4ZADzl+zSbf+D5a3DSnXgWa8ctR673S7wGSE1s9KsWNxRwOCo83OJj02Hj57ZI5/rAKfF5wp/QZC3yKisBn42nbjt7xETwsvqZkGt8Ucj6704vk3zIuxv5uwc8hCeMLRlEe24hE64DcgXRVEfelP6ULrOj 16806@monstermedia.hu\n",
    "known_hosts": "|1|W2L9+jqjnldYmAPhoCCiRW/sNHE=|h62OMpVwTO+xNL/N+MWDUYJ0MAA= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==\n|1|GCIdLCgat3MMIhSQRvyLU1rlv6Q=|/ORkqU2nZyHHSulhnbvOFPWY8yQ= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==\n|1|pIgj0IdUyD1eAoR5YY3QrygT6vY=|0BBOpvAroFHh3Sg2115/ASW6uw4= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==\n"
  },
  "gits": [{
    "branches": [{
      "b_branch": "master",
      "b_pull_single_branch_only": 1,
      "b_blue_green": 0,
      "b_repo_path1": "/git.monstermedia.hu/test/",
      "b_repo_path2": "",
      "b_docroot_subdir": "/subdir/",
      "b_current_repo_index": 0,
      "b_domain_docroot_changes": [{
        "raw": "git.monstermedia.hu/test.git.monstermedia.hu",
        "domain": "git.monstermedia.hu",
        "host": "test.git.monstermedia.hu"
      }],
      "created_at": "2016-12-11 15:39:45",
      "updated_at": "2016-12-11 15:39:45"
    },
    {
      "b_branch": "prod",
      "b_pull_single_branch_only": 1,
      "b_blue_green": 1,
      "b_repo_path1": "/git.monstermedia.hu/prod-blue/",
      "b_repo_path2": "/git.monstermedia.hu/prod-green/",
      "b_docroot_subdir": "/subdir/",
      "b_current_repo_index": 2,
      "b_domain_docroot_changes": [{
        "raw": "git.monstermedia.hu/git.monstermedia.hu",
        "domain": "git.monstermedia.hu",
        "host": "git.monstermedia.hu"
      },
      {
        "raw": "git.monstermedia.hu/www.git.monstermedia.hu",
        "domain": "git.monstermedia.hu",
        "host": "www.git.monstermedia.hu"
      }],
      "created_at": "2016-12-11 15:44:28",
      "updated_at": "2016-12-11 15:44:28"
    }],
    "git": {
      "g_id": "74d3856e9cf41fda92b14edd23d8fa31",
      "g_remote_origin": "git@github.com:monstercloud/myrepo.git",
      "g_name": "myrepo",
      "g_branches": 2,
      "created_at": "2016-12-11 15:39:03",
      "updated_at": "2016-12-11 15:39:03"
    }
  }]
};

  describe("restore", function(){


        it('restoring a specific webstore', function(done) {

                 var oDocroot = app.GetDocroot;
                 app.GetDocroot = standardMockedGetDocroot

             mapi.post( "/wie/"+storage_id, restorePayload, function(err, result, httpResponse){

                assert.equal(result, "ok");

                app.GetDocroot = oDocroot;
                done()

             })

        })


  });




}, 10000)

