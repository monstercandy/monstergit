require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('webhook parsing', function() {    


var bitbucket = {
  "repository": {
    "full_name": "monsterbb\/test",
    "name": "test",
    "links": {
      "html": {
        "href": "https:\/\/bitbucket.org\/monsterbb\/test"
      },
      "self": {
        "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test"
      },
      "avatar": {
        "href": "https:\/\/bitbucket.org\/monsterbb\/test\/avatar\/32\/"
      }
    },
    "uuid": "{ee44bfa3-9115-4473-a35b-e0737d66429b}",
    "website": "",
    "owner": {
      "links": {
        "html": {
          "href": "https:\/\/bitbucket.org\/monsterbb\/"
        },
        "self": {
          "href": "https:\/\/api.bitbucket.org\/2.0\/users\/monsterbb"
        },
        "avatar": {
          "href": "https:\/\/bitbucket.org\/account\/monsterbb\/avatar\/32\/"
        }
      },
      "username": "monsterbb",
      "uuid": "{c684fff1-dfec-432f-a4ef-08559e954212}",
      "display_name": "Imre Rad",
      "type": "user"
    },
    "scm": "git",
    "type": "repository",
    "is_private": true
  },
  "actor": {
    "links": {
      "html": {
        "href": "https:\/\/bitbucket.org\/monsterbb\/"
      },
      "self": {
        "href": "https:\/\/api.bitbucket.org\/2.0\/users\/monsterbb"
      },
      "avatar": {
        "href": "https:\/\/bitbucket.org\/account\/monsterbb\/avatar\/32\/"
      }
    },
    "username": "monsterbb",
    "uuid": "{c684fff1-dfec-432f-a4ef-08559e954212}",
    "display_name": "Imre Rad",
    "type": "user"
  },
  "push": {
    "changes": [
      {
        "links": {
          "commits": {
            "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commits?include=4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c&exclude=ac1fd527d89715611552b4bd311071fb2c22eea8"
          },
          "html": {
            "href": "https:\/\/bitbucket.org\/monsterbb\/test\/branches\/compare\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c..ac1fd527d89715611552b4bd311071fb2c22eea8"
          },
          "diff": {
            "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/diff\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c..ac1fd527d89715611552b4bd311071fb2c22eea8"
          }
        },
        "forced": false,
        "commits": [
          {
            "links": {
              "comments": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c\/comments"
              },
              "html": {
                "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              },
              "diff": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/diff\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              },
              "approve": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c\/approve"
              },
              "patch": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/patch\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              },
              "self": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              },
              "statuses": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c\/statuses"
              }
            },
            "hash": "4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c",
            "date": "2016-11-28T18:36:03+00:00",
            "parents": [
              {
                "links": {
                  "html": {
                    "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/ac1fd527d89715611552b4bd311071fb2c22eea8"
                  },
                  "self": {
                    "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/ac1fd527d89715611552b4bd311071fb2c22eea8"
                  }
                },
                "type": "commit",
                "hash": "ac1fd527d89715611552b4bd311071fb2c22eea8"
              }
            ],
            "message": "teszt\n",
            "author": {
              "raw": "Toshiba\\John Doe <imre.rad@monstermedia.hu>"
            },
            "type": "commit"
          }
        ],
        "truncated": false,
        "created": false,
        "closed": false,
        "old": {
          "target": {
            "links": {
              "html": {
                "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/ac1fd527d89715611552b4bd311071fb2c22eea8"
              },
              "self": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/ac1fd527d89715611552b4bd311071fb2c22eea8"
              }
            },
            "hash": "ac1fd527d89715611552b4bd311071fb2c22eea8",
            "date": "2016-11-28T18:33:07+00:00",
            "parents": [
              {
                "links": {
                  "html": {
                    "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/559a29d25ffbf789183210dc1ce64a97ff0eacae"
                  },
                  "self": {
                    "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/559a29d25ffbf789183210dc1ce64a97ff0eacae"
                  }
                },
                "type": "commit",
                "hash": "559a29d25ffbf789183210dc1ce64a97ff0eacae"
              }
            ],
            "message": "first to qa\n",
            "author": {
              "raw": "Toshiba\\John Doe <imre.rad@monstermedia.hu>"
            },
            "type": "commit"
          },
          "links": {
            "commits": {
              "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commits\/qa"
            },
            "html": {
              "href": "https:\/\/bitbucket.org\/monsterbb\/test\/branch\/qa"
            },
            "self": {
              "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/refs\/branches\/qa"
            }
          },
          "name": "qa",
          "type": "branch"
        },
        "new": {
          "target": {
            "links": {
              "html": {
                "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              },
              "self": {
                "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c"
              }
            },
            "hash": "4b9e1c0d2d06c3bdcc0e40870ed383750f84ed9c",
            "date": "2016-11-28T18:36:03+00:00",
            "parents": [
              {
                "links": {
                  "html": {
                    "href": "https:\/\/bitbucket.org\/monsterbb\/test\/commits\/ac1fd527d89715611552b4bd311071fb2c22eea8"
                  },
                  "self": {
                    "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commit\/ac1fd527d89715611552b4bd311071fb2c22eea8"
                  }
                },
                "type": "commit",
                "hash": "ac1fd527d89715611552b4bd311071fb2c22eea8"
              }
            ],
            "message": "teszt\n",
            "author": {
              "raw": "Toshiba\\John Doe <imre.rad@monstermedia.hu>"
            },
            "type": "commit"
          },
          "links": {
            "commits": {
              "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/commits\/qa"
            },
            "html": {
              "href": "https:\/\/bitbucket.org\/monsterbb\/test\/branch\/qa"
            },
            "self": {
              "href": "https:\/\/api.bitbucket.org\/2.0\/repositories\/monsterbb\/test\/refs\/branches\/qa"
            }
          },
          "name": "qa",
          "type": "branch"
        }
      }
    ]
  }
}

var github = {
  "ref": "refs\/heads\/qa",
  "before": "44e5344d3a8378b92a09bc6f2114dc8234030c48",
  "after": "65d3121983fc939d55855fb224cdafd25c139004",
  "created": false,
  "deleted": false,
  "forced": false,
  "base_ref": null,
  "compare": "https:\/\/github.com\/irsl\/test\/compare\/44e5344d3a83...65d3121983fc",
  "commits": [
    {
      "id": "65d3121983fc939d55855fb224cdafd25c139004",
      "tree_id": "cfc7caf4a9305d1298c5dbefa8744a56d9bf214e",
      "distinct": true,
      "message": "test commit",
      "timestamp": "2016-11-28T19:27:37+01:00",
      "url": "https:\/\/github.com\/irsl\/test\/commit\/65d3121983fc939d55855fb224cdafd25c139004",
      "author": {
        "name": "Toshiba\\John Doe",
        "email": "imre.rad@monstermedia.hu"
      },
      "committer": {
        "name": "Toshiba\\John Doe",
        "email": "imre.rad@monstermedia.hu"
      },
      "added": [
        
      ],
      "removed": [
        
      ],
      "modified": [
        "qa.txt"
      ]
    }
  ],
  "head_commit": {
    "id": "65d3121983fc939d55855fb224cdafd25c139004",
    "tree_id": "cfc7caf4a9305d1298c5dbefa8744a56d9bf214e",
    "distinct": true,
    "message": "test commit",
    "timestamp": "2016-11-28T19:27:37+01:00",
    "url": "https:\/\/github.com\/irsl\/test\/commit\/65d3121983fc939d55855fb224cdafd25c139004",
    "author": {
      "name": "Toshiba\\John Doe",
      "email": "imre.rad@monstermedia.hu"
    },
    "committer": {
      "name": "Toshiba\\John Doe",
      "email": "imre.rad@monstermedia.hu"
    },
    "added": [
      
    ],
    "removed": [
      
    ],
    "modified": [
      "qa.txt"
    ]
  },
  "repository": {
    "id": 75000554,
    "name": "test",
    "full_name": "irsl\/test",
    "owner": {
      "name": "irsl",
      "email": "irsl@users.noreply.github.com"
    },
    "private": false,
    "html_url": "https:\/\/github.com\/irsl\/test",
    "description": "test",
    "fork": false,
    "url": "https:\/\/github.com\/irsl\/test",
    "forks_url": "https:\/\/api.github.com\/repos\/irsl\/test\/forks",
    "keys_url": "https:\/\/api.github.com\/repos\/irsl\/test\/keys{\/key_id}",
    "collaborators_url": "https:\/\/api.github.com\/repos\/irsl\/test\/collaborators{\/collaborator}",
    "teams_url": "https:\/\/api.github.com\/repos\/irsl\/test\/teams",
    "hooks_url": "https:\/\/api.github.com\/repos\/irsl\/test\/hooks",
    "issue_events_url": "https:\/\/api.github.com\/repos\/irsl\/test\/issues\/events{\/number}",
    "events_url": "https:\/\/api.github.com\/repos\/irsl\/test\/events",
    "assignees_url": "https:\/\/api.github.com\/repos\/irsl\/test\/assignees{\/user}",
    "branches_url": "https:\/\/api.github.com\/repos\/irsl\/test\/branches{\/branch}",
    "tags_url": "https:\/\/api.github.com\/repos\/irsl\/test\/tags",
    "blobs_url": "https:\/\/api.github.com\/repos\/irsl\/test\/git\/blobs{\/sha}",
    "git_tags_url": "https:\/\/api.github.com\/repos\/irsl\/test\/git\/tags{\/sha}",
    "git_refs_url": "https:\/\/api.github.com\/repos\/irsl\/test\/git\/refs{\/sha}",
    "trees_url": "https:\/\/api.github.com\/repos\/irsl\/test\/git\/trees{\/sha}",
    "statuses_url": "https:\/\/api.github.com\/repos\/irsl\/test\/statuses\/{sha}",
    "languages_url": "https:\/\/api.github.com\/repos\/irsl\/test\/languages",
    "stargazers_url": "https:\/\/api.github.com\/repos\/irsl\/test\/stargazers",
    "contributors_url": "https:\/\/api.github.com\/repos\/irsl\/test\/contributors",
    "subscribers_url": "https:\/\/api.github.com\/repos\/irsl\/test\/subscribers",
    "subscription_url": "https:\/\/api.github.com\/repos\/irsl\/test\/subscription",
    "commits_url": "https:\/\/api.github.com\/repos\/irsl\/test\/commits{\/sha}",
    "git_commits_url": "https:\/\/api.github.com\/repos\/irsl\/test\/git\/commits{\/sha}",
    "comments_url": "https:\/\/api.github.com\/repos\/irsl\/test\/comments{\/number}",
    "issue_comment_url": "https:\/\/api.github.com\/repos\/irsl\/test\/issues\/comments{\/number}",
    "contents_url": "https:\/\/api.github.com\/repos\/irsl\/test\/contents\/{+path}",
    "compare_url": "https:\/\/api.github.com\/repos\/irsl\/test\/compare\/{base}...{head}",
    "merges_url": "https:\/\/api.github.com\/repos\/irsl\/test\/merges",
    "archive_url": "https:\/\/api.github.com\/repos\/irsl\/test\/{archive_format}{\/ref}",
    "downloads_url": "https:\/\/api.github.com\/repos\/irsl\/test\/downloads",
    "issues_url": "https:\/\/api.github.com\/repos\/irsl\/test\/issues{\/number}",
    "pulls_url": "https:\/\/api.github.com\/repos\/irsl\/test\/pulls{\/number}",
    "milestones_url": "https:\/\/api.github.com\/repos\/irsl\/test\/milestones{\/number}",
    "notifications_url": "https:\/\/api.github.com\/repos\/irsl\/test\/notifications{?since,all,participating}",
    "labels_url": "https:\/\/api.github.com\/repos\/irsl\/test\/labels{\/name}",
    "releases_url": "https:\/\/api.github.com\/repos\/irsl\/test\/releases{\/id}",
    "deployments_url": "https:\/\/api.github.com\/repos\/irsl\/test\/deployments",
    "created_at": 1480356624,
    "updated_at": "2016-11-28T18:10:24Z",
    "pushed_at": 1480357663,
    "git_url": "git:\/\/github.com\/irsl\/test.git",
    "ssh_url": "git@github.com:irsl\/test.git",
    "clone_url": "https:\/\/github.com\/irsl\/test.git",
    "svn_url": "https:\/\/github.com\/irsl\/test",
    "homepage": null,
    "size": 0,
    "stargazers_count": 0,
    "watchers_count": 0,
    "language": null,
    "has_issues": true,
    "has_downloads": true,
    "has_wiki": true,
    "has_pages": false,
    "forks_count": 0,
    "mirror_url": null,
    "open_issues_count": 1,
    "forks": 0,
    "open_issues": 1,
    "watchers": 0,
    "default_branch": "master",
    "stargazers": 0,
    "master_branch": "master"
  },
  "pusher": {
    "name": "irsl",
    "email": "irsl@users.noreply.github.com"
  },
  "sender": {
    "login": "irsl",
    "id": 6357121,
    "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/6357121?v=3",
    "gravatar_id": "",
    "url": "https:\/\/api.github.com\/users\/irsl",
    "html_url": "https:\/\/github.com\/irsl",
    "followers_url": "https:\/\/api.github.com\/users\/irsl\/followers",
    "following_url": "https:\/\/api.github.com\/users\/irsl\/following{\/other_user}",
    "gists_url": "https:\/\/api.github.com\/users\/irsl\/gists{\/gist_id}",
    "starred_url": "https:\/\/api.github.com\/users\/irsl\/starred{\/owner}{\/repo}",
    "subscriptions_url": "https:\/\/api.github.com\/users\/irsl\/subscriptions",
    "organizations_url": "https:\/\/api.github.com\/users\/irsl\/orgs",
    "repos_url": "https:\/\/api.github.com\/users\/irsl\/repos",
    "events_url": "https:\/\/api.github.com\/users\/irsl\/events{\/privacy}",
    "received_events_url": "https:\/\/api.github.com\/users\/irsl\/received_events",
    "type": "User",
    "site_admin": false
  }
}

//https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/web_hooks/web_hooks.md
var gitlab = {
  "object_kind": "push",
  "before": "95790bf891e76fee5e1747ab589903a6a1f80f22",
  "after": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
  "ref": "refs/heads/master",
  "checkout_sha": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
  "user_id": 4,
  "user_name": "John Smith",
  "user_email": "john@example.com",
  "user_avatar": "https://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=8://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=80",
  "project_id": 15,
  "project":{
    "name":"Diaspora",
    "description":"",
    "web_url":"http://example.com/mike/diaspora",
    "avatar_url":null,
    "git_ssh_url":"git@example.com:mike/diaspora.git",
    "git_http_url":"http://example.com/mike/diaspora.git",
    "namespace":"Mike",
    "visibility_level":0,
    "path_with_namespace":"mike/diaspora",
    "default_branch":"master",
    "homepage":"http://example.com/mike/diaspora",
    "url":"git@example.com:mike/diaspora.git",
    "ssh_url":"git@example.com:mike/diaspora.git",
    "http_url":"http://example.com/mike/diaspora.git"
  },
  "repository":{
    "name": "Diaspora",
    "url": "git@example.com:mike/diaspora.git",
    "description": "",
    "homepage": "http://example.com/mike/diaspora",
    "git_http_url":"http://example.com/mike/diaspora.git",
    "git_ssh_url":"git@example.com:mike/diaspora.git",
    "visibility_level":0
  },
  "commits": [
    {
      "id": "b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
      "message": "Update Catalan translation to e38cb41.",
      "timestamp": "2011-12-12T14:27:31+02:00",
      "url": "http://example.com/mike/diaspora/commit/b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
      "author": {
        "name": "Jordi Mallach",
        "email": "jordi@softcatala.org"
      },
      "added": ["CHANGELOG"],
      "modified": ["app/controller/application.rb"],
      "removed": []
    },
    {
      "id": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
      "message": "fixed readme",
      "timestamp": "2012-01-03T23:36:29+02:00",
      "url": "http://example.com/mike/diaspora/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
      "author": {
        "name": "GitLab dev user",
        "email": "gitlabdev@dv6700.(none)"
      },
      "added": ["CHANGELOG"],
      "modified": ["app/controller/application.rb"],
      "removed": []
    }
  ],
  "total_commits_count": 4
}



   const GitLib = require("models/git.js")
   it("github", function(done){
   	   var x = GitLib.GetBranchFiltersBasedOnWebhook(github)
   	   assert.equal(x, "qa")
   	   done()
   })

   it("bitbucket", function(done){
   	   var x = GitLib.GetBranchFiltersBasedOnWebhook(bitbucket)
   	   assert.equal(x, "qa")
   	   done()
   })

   it("gitlab", function(done){
   	   var x = GitLib.GetBranchFiltersBasedOnWebhook(gitlab)
   	   assert.equal(x, "master")
   	   done()
   })

})

describe("domain/host parsing", function(){

     const branch = require("models/branch.js")

        it('branch model should be able to correctly decode domain/host strings', function(done) {
             var re = branch.DomainHostStringsToHash(["domain.hu/www.domain.hu", "domain.hu/foobar.domain.hu"])
             assert.deepEqual([ { raw: 'domain.hu/www.domain.hu',
    domain: 'domain.hu',
    host: 'www.domain.hu' },
  { raw: 'domain.hu/foobar.domain.hu',
    domain: 'domain.hu',
    host: 'foobar.domain.hu' } ],re)
             done()

        })


        it('branch model should throw with invalid domain/host strings', function(done) {
             try{
                branch.DomainHostStringsToHash(["domain.hu/../foobar.domain.hu"])
                done(new Error("should have failed"))
             }catch(ex){
                done()
             }

        })



})