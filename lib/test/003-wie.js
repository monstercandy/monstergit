require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../git-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const storage_id = 32345

    const fs = require("MonsterDotq").fs();

    var latestBackup;
    var getInfoCalls = 0;
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){

                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id,
                     wh_user_id: 123,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_git_max_repos: 3,
                       t_git_max_branches: 3,
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }

    const expectedBackup = {
       ssh: {
          "config": "#{\"StrictHostKeyChecking\":false}\nStrictHostKeyChecking false\n",
          "id_rsa.pub": "public rsa key"
       },
       gits: [
         {
          git: {
           g_remote_origin: 'https://github.com/irsl/dfwfw.git',
           g_name: 'myname',
           g_branches: 1,
          },
          branches: [ { b_branch: 'dev',
    b_pull_single_branch_only: 0,
    b_blue_green: 1,
    b_repo_path1: '/foo/dev/blue/',
    b_repo_path2: '/foo/dev/green/',
    b_docroot_subdir: '/',
    b_current_repo_index: 0,
    b_domain_docroot_changes: [
                  {
                    "domain": "domain1.hu",
                    "host": "www.domain1.hu",
                    "raw": "domain1.hu/www.domain1.hu",
                  }
     ],
    } ]
         }
      ]
    };

    const rsapubContent = "public rsa key";

    const git_good_config = {g_name:"myname", g_remote_origin: "https://github.com/irsl/dfwfw.git", }
    const branch_good_config = {dont_clone: true, b_pull_single_branch_only:false, b_domain_docroot_changes: ["domain1.hu/www.domain1.hu"], b_blue_green: true, b_repo_path1: "/foo/dev/blue", b_repo_path2: "/foo/dev/green", b_branch: "dev" }

     const standardMockedGetDocroot = function(){
                 return {
                    getAsync: function(path){
                        assert.equal(path, "/s/[this]/docrootapi/docroots/hostentries/"+storage_id)
                        return Promise.resolve({result:
                            [
                             {raw: "domain1.hu/www.domain1.hu", domain: "domain1.hu", host: "www.domain1.hu"},
                             {raw: "domain2.hu/www.domain2.hu", domain: "domain2.hu", host: "www.domain2.hu"},
                            ]
                        })
                    }
                 }
             }

	describe('basic tests', function() {

        it('insert git, should work', function(done) {

             mapi.put( "/"+storage_id, git_good_config, function(err, result, httpResponse){
                assert.property(result, "g_id")

                g_id = result.g_id

                done()

             })

        })


            it('insert branch, should work', function(done) {

                 var oDocroot = app.GetDocroot;
                 app.GetDocroot = standardMockedGetDocroot


                 mapi.put( "/"+storage_id+"/name/myname/branches", branch_good_config, function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    app.GetDocroot = oDocroot;

                    done()

                 })

            })


            it("creating ssh deployment key manually", function(){
                    const rsapriv = 'ssh-keys.d\\'+storage_id+'\\.ssh\\id_rsa'

                    return fs.writeFileAsync(rsapriv+".pub", rsapubContent)
            })


	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

      cleanup();

        it('get list of gits should be empty', function(done) {

             mapi.get( "/"+storage_id,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

                 var oDocroot = app.GetDocroot;
                 app.GetDocroot = standardMockedGetDocroot

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");

                app.GetDocroot = oDocroot;
                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){
             mapi.delete( "/"+storage_id, {}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                // console.log(result.gits[0].branches);

                latestBackup = simpleCloneObject(result);

                var actual = result;
                actual.gits.forEach(row=>{
                   Array("g_id", "updated_at", "created_at").forEach(x=>{
                      assert.ok(row.git[x]);
                      delete row.git[x];
                   })

                   assert.notProperty(row, "g_user_id");

                   row.branches.forEach(branch=>{
                       Array("updated_at", "created_at").forEach(x=>{
                          assert.ok(branch[x]);
                          delete branch[x];
                       })

                       assert.notProperty(row, "b_git_id");
                       assert.notProperty(row, "b_id");
                       assert.notProperty(row, "b_webhosting");
                   })
                })

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

