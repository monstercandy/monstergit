var lib = module.exports = function(app, knex) {

   const gitLib = require("models/git.js")
   const branchLib = require("models/branch.js")

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.BackupWh = function(wh, req) {
      var re = {};
      var git = gitLib(wh, knex, app);
      return git.ReadSshFiles()
        .catch(ex=>{
            if(ex.code == "ENOENT") return;
        })
        .then(files=>{
           if(files)
             re.ssh = files;
           return git.GetAllGitsOfWebhosting();
        })
        .then(gits=>{
           re.gits = [];

           return dotq.linearMap({array: gits, action: git=>{
              var branch = branchLib(git, knex, app);

              return branch.GetBranches()
                .then(branches=>{
                    var a = {branches: branches};

                    branches.forEach(branch=>{
                       Array("b_id", "b_webhosting", "b_git_id").forEach(x=>{
                          delete branch[x];
                       })
                    })

                    Array("g_user_id", "g_webhosting", "webhook_url").forEach(x=>{
                       delete git[x];
                    })

                    a.git = git;
                    re.gits.push(a);
                })

           }})

        })
        .then(()=>{
           return re;
        })
   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knex.transaction(function(trx){
          var wieTrx = require("Wie").transformWie(app, lib(app, trx));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){

      return knex.raw("SELECT DISTINCT g_webhosting FROM gits")
        .then(sites=>{

            var re = [];
            sites.forEach(site=>{
               re.push(site.g_webhosting);
            })
            return re;
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      // lets put the site first
      var git = gitLib(wh, knex, app);
      return Promise.resolve()
        .then(()=>{
           if(!in_data.ssh) return;

           return git.RestoreSshFiles(in_data.ssh)
        })
        .then(()=>{
             return  knex.transaction(function(trx){
                return dotq.linearMap({array: in_data.gits, action: git_data=>{
                    git_data.git.updated_at = moment.now();

                    return git.Insert(git_data.git, req)
                      .catch(ex=>{
                          if(ex.message != "NAME_ALREADY_EXISTS")
                            throw ex;

                           // otherwise this is fine
                           return {g_id: git_data.git.g_id};
                      })
                      .then((gre)=>{
                         return git.GetGitBy_id(gre.g_id);
                      })
                      .then(git=>{
                          var branch = branchLib(git, knex, app);

                          return dotq.linearMap({array: git_data.branches, action: branch_data=>{
                             branch_data.dont_clone = true;
                             return branch.Insert(branch_data, req)
                               .catch(ex=>{
                                    if(ex.message != "BRANCH_ALREADY_EXISTS")
                                      throw ex;
                               })
                          }})
                      })

                }})
              })

        })
        .then(()=>{
            app.InsertEvent(req, {e_event_type: "restore-wh-gits", e_other: wh.wh_id})

        })

   }



   return wie;

}
