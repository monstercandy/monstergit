FROM monstercommon

ADD lib /opt/MonsterGit/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterGit/lib/bin/git-install.sh && /opt/MonsterGit/lib/bin/git-test.sh

ENTRYPOINT ["/opt/MonsterGit/lib/bin/git-start.sh"]
